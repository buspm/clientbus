import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    username: null,
    password: null,
    email: null,
    nama: null,
    jenis_kelamin: '',
    foto: null
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSelectedFile = event => {
    this.setState({
      foto: event.target.files[0]
    });
  };

  create = () => {
    const data = new FormData();
    data.append('foto', this.state.foto, this.state.foto.name);
    data.append('username', this.state.username);
    data.append('password', this.state.password);
    data.append('email', this.state.email);
    data.append('nama', this.state.nama);
    data.append('jenis_kelamin', this.state.jenis_kelamin);

    axios.post('http://localhost:2018/admin', data).then(res => {
      this.setState({
        open: false,
        username: null,
        password: null,
        email: null,
        nama: null,
        jenis_kelamin: '',
        foto: null
      });
      this.props.getData();
    });
  };

  dataForm = [
    {
      title: 'Usename',
      name: 'username',
      nilai: this.state.username
    },
    {
      title: 'Password',
      name: 'password',
      nilai: this.state.password
    },
    {
      title: 'Email',
      name: 'email',
      nilai: this.state.email
    },
    {
      title: 'Nama',
      name: 'nama',
      nilai: this.state.nama
    }
  ];

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              {this.dataForm.map((datas, index) => {
                return (
                  <TextField
                    key={index}
                    id="filled-name"
                    label={datas.title}
                    className={classes.textField}
                    name={datas.name}
                    value={datas.value}
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="filled"
                  />
                );
              })}
              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="jenis_kelamin">Jenis Kelamin</InputLabel>
                <Select
                  value={this.state.jenis_kelamin}
                  onChange={this.handleChange}
                  input={
                    <FilledInput name="jenis_kelamin" id="jenis_kelamin" />
                  }>
                  <MenuItem value="l">Laki Laki</MenuItem>
                  <MenuItem value="p">Perempuan</MenuItem>
                </Select>
              </FormControl>
              <input type="file" onChange={this.handleSelectedFile} />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button onClick={this.create} color="primary" autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
