import React, { Component, Fragment } from 'react';

import { Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/';
import Dashboard from './components/Dashboard';
import Home from './containers/home/Home';

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/" component={Dashboard} />
      </Switch>
    );
  }
}

export default App;
